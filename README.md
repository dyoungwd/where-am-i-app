# README #



## Where Am I ###

Where Am I is a GPS web app built using Framework 7, based on Angular JS and utilises the Google Map API to find your geolocation to display the geographic location of a user or device on a Google map, using HTML5 Geolocation features along with the Google Maps JavaScript API.

WHAT IS GEOLOCATION?

Geolocation refers to the identification of the geographic location of a user or computing device via a variety of data collection mechanisms. Typically, most geolocation services use network routing addresses or internal GPS devices to determine this location. Geolocation is a device-specific API. This means that browsers or devices must support geolocation in order to use it through web applications.

## Screenshots
![enter image description here](https://objects-us-west-1.dream.io/bisdev/where-am-i/screenshot/default-page.png "Default Screen")

![enter image description here](https://objects-us-west-1.dream.io/bisdev/where-am-i/screenshot/location-page.png "Location Screen")
### LICENCE
This App is licenced under MIT.


### Version 1.1
Changed framework to use Framework 7 and Angular JS
### Version 1.0
A GPS web app built using jQuery Mobile, utilising Google Map API


### Web Demo###

http://bisdev.space/where-am-i/
